class HomePage

  LOGIN_WINDOW = {css: 'li.header__user__account[data-authen="0"] :first-child'}
  CART_WINDOW = {css: '.header__cart__text'}

  attr_reader :driver

  def initialize(driver)
    @driver = driver
  end

  def open_login_page
    driver.find_element(LOGIN_WINDOW).click
  end

  def open_cart
    driver.find_element(CART_WINDOW).click
  end

  def open_link(link)
    driver.get link
  end

  def visit
    driver.get ENV['base_url']
  end

  private

  def wait_for(seconds=5)
    Selenium::WebDriver::Wait.new(:timeout => seconds).until { yield }
  end

  def displayed?(locator)
    driver.find_element(locator).displayed?
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
end


class Product
  attr_accessor :name, :price, :link
end