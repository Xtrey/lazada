require_relative 'home_page'
class Cart < HomePage

  CART_PRODUCTS = {css: '#cart-items-list-form div.scrollable table'}
  CART_TOTAL = {css: '#subtotal tr.total td div'}
  CART_PRODUCT_DESCRIPTION = {css: '.productdescription'}
  CART_PRODUCT_PRICE = {css: 'td[class~="price"] span'}
  CART_PRODUCT_TOTAL_PRICE = {css: 'td[class~="lastcolumn"]'}
  PRODUCT_QTY = {css: 'select'}

  attr_reader :driver

  def products_in_cart?(products_expected)
    products_actual = Hash.new
    products_raw = driver.find_elements(CART_PRODUCTS)
    products_raw.each do |item|
      products_actual[item.find_element(CART_PRODUCT_DESCRIPTION).text] = item.find_element(CART_PRODUCT_PRICE).text
    end
    products_expected_hash = Hash.new
    products_expected.each {|item| products_expected_hash[item.name] = item.price}
    products_expected_hash == products_actual
  end

  def change_product_qty(product, qty)
    products = driver.find_elements(CART_PRODUCTS)
    products.each do |item|
      if item.find_element(CART_PRODUCT_DESCRIPTION).text == product.name
        option = Selenium::WebDriver::Support::Select.new(item.find_element(PRODUCT_QTY))
        option.select_by(:text, qty.to_s)
      end
    end
  end

  def get_product_price(product)
    products = driver.find_elements(CART_PRODUCTS)
    products.each do |item|
      if item.find_element(CART_PRODUCT_DESCRIPTION).text == product.name
        return item.find_element(CART_PRODUCT_PRICE).text
      end
    end
  end

  def get_product_total_price(product)
    products = driver.find_elements(CART_PRODUCTS)
    products.each do |item|
      if item.find_element(CART_PRODUCT_DESCRIPTION).text == product.name
        return item.find_element(CART_PRODUCT_TOTAL_PRICE).text
      end
    end
  end

  def get_total_price
    driver.find_element(CART_TOTAL).text
  end

end