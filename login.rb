require_relative 'home_page'
class Login < HomePage

  LOGIN_EMAIL = {css: 'input[name="LoginForm[email]"]'}
  LOGIN_PASSWORD = {css: 'input[name="LoginForm[password]"]'}
  LOGIN_ERROR = {css: 'span[class~=error-display]'}
  LOGIN_SUBMIT = {css: 'button[class~="popup-login-but"]'}
  LOGIN_CLOSE = {css: 'a[title~="close"]'}

  attr_reader :driver

  def login(user=ENV['user'], password=ENV['password'])
    driver.find_element(LOGIN_EMAIL).send_keys user
    driver.find_element(LOGIN_PASSWORD).send_keys password
    driver.find_element(LOGIN_SUBMIT).click
  end

  def close_login_page
    driver.find_element(LOGIN_CLOSE).click
  end

  def is_login_fail?(return_error=false)
    if displayed?(LOGIN_ERROR)
      if return_error
        driver.find_element(LOGIN_ERROR).text
      else
        true
      end
    else
      false
    end
  end

end