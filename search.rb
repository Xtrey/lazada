require_relative 'home_page'
class Search < HomePage

  SEARCH_BOX = {css: '#search input' }
  SEARCH_BOX_SUBMIT = {css: '#search div'}
  SEARCH_RESULT = {css: '.catalog__title'}
  SEARCH_PRODUCT_LIST = {css: '.product-card'}
  PRODUCT_CARD = {css: '.product-card'}
  PRODUCT_TITLE = {css: '.product-card__img img'}
  PRODUCT_PRICE = {css: '.product-card__price'}
  ADD_TO_CART = {css: '#AddToCart'}
  CART_WINDOW = {css: '#cart-items-list-form'}

  attr_reader :driver

  def search(search_query)
    driver.find_element(SEARCH_BOX).clear
    driver.find_element(SEARCH_BOX).send_keys search_query
    driver.find_element(SEARCH_BOX_SUBMIT).click
  end


  def get_list_products
    products = []
    products_raw = driver.find_elements(PRODUCT_CARD)
    products_raw.each { |item|
      product = Product.new
      product.name = item.find_element(PRODUCT_TITLE).attribute('title')
      product.price = item.find_element(PRODUCT_PRICE).text
      product.link = item.attribute('data-original')
      products << product
    }
    products
  end

  def add_to_cart(product)
    open_link(product.link)
    driver.find_element(ADD_TO_CART).click
    wait_for(30) {displayed?(CART_WINDOW)}
  end

end

