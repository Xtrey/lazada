require 'selenium-webdriver'
require_relative 'login'
require_relative 'search'
require_relative 'cart'

def setup
  @driver = Selenium::WebDriver.for :firefox
  ENV['base_url'] = 'http://www.lazada.vn/?setLang=en'
  ENV['user'] = 'test@test.com'
  ENV['password'] = 'test'
end

def teardown
  @driver.quit
end

def run
  setup
  yield
  teardown
end


run {
  # Test case 1
  lazada = Login.new(@driver)
  lazada.visit
  lazada.open_login_page
  lazada.login(user=ENV['user'], password='')
  result = lazada.is_login_fail?(return_error=true)
  lazada.close_login_page
  sleep(5)
  if result  == 'Required field'
    puts 'Test case 1 passed'
  else
    puts 'Test case 1 failed'
  end

  #Test case 2
  lazada_search = Search.new(@driver)
  lazada_search.search('lenovo')
  products = lazada_search.get_list_products
  added_products = products.sample(2)
  added_products.each { |item| lazada_search.add_to_cart(item)}
  lazada_cart = Cart.new(@driver)
  sleep(5)
  if lazada_cart.products_in_cart?(added_products)
    puts 'Test case 2 passed'
  else
    puts 'Test case 2 failed'
  end

  # Test case 3
  result = lazada_cart.get_product_total_price(added_products[0])
  if result == added_products[0].price
    puts 'Test case 3 passed'
  else
    puts 'Test case 3 failed'
  end

  #Test case 4
  lazada_cart.change_product_qty(added_products[0], 2)
  total = added_products[0].price.gsub('.', '').to_i * 2
  sleep(5)
  result = lazada_cart.get_product_total_price(added_products[0]).gsub('.', '').to_i
  if result == total
    puts 'Test case 4 passed'
  else
    puts 'Test case 4 failed'
  end
}